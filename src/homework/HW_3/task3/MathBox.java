package homework.HW_3.task3;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MathBox<T extends Number> extends ObjectBox{
    /**
     * Написать класс MathBox, реализующий следующий функционал:
     *
     * Конструктор на вход получает массив Number. Элементы не могут повторяться. Элементы массива внутри объекта раскладываются в подходящую коллекцию (выбрать самостоятельно).
     * Существует метод summator, возвращающий сумму всех элементов коллекции.
     * Существует метод splitter, выполняющий поочередное деление всех хранящихся в объекте элементов на делитель, являющийся аргументом метода. Хранящиеся в объекте данные полностью заменяются результатами деления.
     * Необходимо правильно переопределить методы toString, hashCode, equals, чтобы можно было использовать MathBox для вывода данных на экран и хранение объектов этого класса в коллекциях (например, hashMap). Выполнение контракта обязательно!
     * Создать метод, который получает на вход Integer и если такое значение есть в коллекции, удаляет его.
     */

    public static void main(String[] args) {
        Number[] arr = {1,2,3,4.4,4.4,9};
        MathBox mb = new MathBox<>(arr);
        //Number sum = mb.summator();
        //System.out.println("Sum = " + sum);
        //mb.splitter(2);
        //mb.deleteObject(4);
        System.out.println(mb.dump());
        System.out.println(mb.toString());
        Object m = 5.2;
        mb.addObject(m);
        System.out.println(mb.dump());
        System.out.println(mb.toString());
    }

    private Set<T> setArr;

    public MathBox(T[] arr){
        /**
         * Конструктор на вход получает массив. Элементы не могут повторяться. Элементы массива внутри объекта раскладываются в коллекцию.
         */
        super(arr);
        setArr = super.getSetObj();
    }

    public Number summator(){
        /**
         * Возвращающий сумму всех элементов коллекции
         */
        Number sum = 0;
        for (Number n : setArr){
            sum = sum.doubleValue() + n.doubleValue();
        }
        return sum;
    }

    public void splitter(T sp){
        /**
         * Выполняет поочередное деление всех хранящихся в объекте элементов на делитель, являющийся аргументом метода. Хранящиеся в объекте данные полностью заменяются результатами деления.
         */
        Set<Double> tempSetArr = new HashSet<>();
        for (Number n : setArr){
            tempSetArr.add(n.doubleValue()/sp.doubleValue());
        }
        setArr = (Set<T>) tempSetArr;
    }

    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        for (Number n : setArr){
            str.append(n);
            str.append(", ");
        }
        str.deleteCharAt(str.length()-2);
        return str.toString();
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        for (Number n : setArr){
            result = prime * result + n.intValue();
        }
        return result;
    }

    @Override
    public boolean equals(Object obj){
        if (obj == this) {
            System.out.println(1);
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()){
            System.out.println(2);
            return false;
        }
        MathBox t = (MathBox) obj;
        System.out.println(setArr);
        System.out.println(t.setArr);
        if (setArr != t.setArr) {
            System.out.println(3);
            return false;
        }
        return true;
    }

    public void addObject(T obj) {
        try {
            if (obj instanceof Object){
                throw new Exception();
            }
            else setArr.add(obj);
        }
        catch (Exception e) {
            System.out.println("Нет возможности добавить объект клааса Object");
        }

    }
}
