package homework.HW_5.entity;

import java.util.Locale;
import java.util.Random;
import java.util.UUID;

public class Pet {

    private UUID id;
    private String nickname;
    private Person owner;
    private float weight;

    public Pet(String nickname, Person owner, float weight){
        this.id = UUID.randomUUID();
        this.nickname = nickname;
        this.owner = owner;
        this.weight = weight;
    }

    static Pet getRandPet() {
        /**
         * метод для получения случайного питомца
         */
        String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lower = upper.toLowerCase(Locale.ROOT);
        StringBuilder randNickname = new StringBuilder();
        Random rnd = new Random();
        int length = rnd.nextInt(8) + 2;
        int index = (int) (rnd.nextFloat() * upper.length());
        randNickname.append(upper.charAt(index));
        while (randNickname.length() < length) {
            index = (int) (rnd.nextFloat() * lower.length());
            randNickname.append(lower.charAt(index));
        }
        Person randOwner = Person.getRandPerson();
        Pet p = new Pet(randNickname.toString(), randOwner, (rnd.nextInt(100)) + ((float)rnd.nextInt(10)/10));
        return p;
    }

    public static Pet[] CreatePets(int N) {
        /**
         * генерация коллекции питомцев
         */
        Pet[] p = new Pet[N];
        for (int i = 0; i < N; i++) {
            p[i] = getRandPet();
        }
        return p;
    }

    public void setNicknamePet (String nickname){
        this.nickname = nickname;
    }

    public void setOwnerPet (Person owner){
        this.owner = owner;
    }

    public void setWeightPet (float weight){
        this.weight = weight;
    }

    public UUID getId(){
        return this.id;
    }

    public String getNickname(){
        return this.nickname;
    }

    public Person getOwner(){
        return this.owner;
    }

    public float getWeight(){
        return this.weight;
    }

    public void showPet(){
        /**
         * вывод информации о питомце
         */
        System.out.println("Хозяин: " + this.owner.getName() + " " + this.owner.getSex() + " " + this.owner.getAge() +
                "; id - " + this.id.toString() + "; кличка питомца - " + this.nickname + "; вес - " + this.weight + " кг.");
    }
}
