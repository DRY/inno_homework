package homework.HW_6.task1;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task1 {
    /**
     * Написать программу, читающую текстовый файл.
     * Программа должна составлять отсортированный по алфавиту список слов, найденных в файле и сохранять его в файл-результат.
     * Найденные слова не должны повторяться, регистр не должен учитываться.
     * Одно слово в разных падежах – это разные слова.
     */
    public static void main(String[] args) {
        Set stringSet = readFile("in.txt");
        writeFile(stringSet, "out.txt");
    }

    public static Set readFile(String file) {
        /**
         * Метод чтения файла и возвращающий коллекцию слов
         */
        Set<String> setFile = new TreeSet<>();
        try (BufferedReader brFile = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"))) {
            String sCurrentLine;

            while ((sCurrentLine = brFile.readLine()) != null) {
                String[] words = sCurrentLine.split(" ");
                for (int i = 0; i < words.length; i++) {
                    setFile.add(getOnlyStrings(words[i]).toLowerCase());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return setFile;
    }

    public static void writeFile(Set<String> strFile, String file) {
        /**
         * Мектод записи слов в файл-результат
         */
        try (BufferedWriter bwFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF8"))) {
            for (String s : strFile) {
                bwFile.write(s + "\n");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getOnlyStrings(String s) {
        /**
         * Метод, который возвращает слово без знаков или спецсимволов
         */
        Pattern pattern = Pattern.compile("[^a-z A-Z а-я А-Я]");
        Matcher matcher = pattern.matcher(s);
        String number = matcher.replaceAll("");
        return number;
    }
}
