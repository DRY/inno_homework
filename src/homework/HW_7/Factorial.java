package homework.HW_7;

import java.math.BigInteger;
import java.util.Set;
import java.util.concurrent.Callable;

public class Factorial implements Callable<BigInteger> {

    int num;

    public Factorial(int i){
        num = i;
    }

    @Override
    public BigInteger call() {
        /**
         * Выполняет вычисление факториала
         */
        BigInteger result = BigInteger.valueOf(1);
        Set<Integer> keys = Main.map.keySet();
        int j = 1;
        for (int i = 1; i <= num; i++) {
            for (int k : keys) {
                if (k == i) {
                    result = Main.map.get(k);
                    j = k + 1;
                }
            }
        }
        for (; j <= num; j++) {
            result = result.multiply(BigInteger.valueOf(j));
        }
        Main.map.put(num, result);
        return result;
    }
}
