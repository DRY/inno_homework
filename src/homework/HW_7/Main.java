package homework.HW_7;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.concurrent.*;

public class Main {
    /**
     * Дан массив случайных чисел. Написать программу для вычисления факториалов всех элементов массива. Использовать пул потоков для решения задачи.
     *
     * Особенности выполнения:
     *
     * Для данного примера использовать рекурсию - не очень хороший вариант, т.к. происходит большое выделение памяти, очень вероятен StackOverFlow.
     * Лучше перемножать числа в простом цикле при этом создавать объект типа BigInteger
     *
     * По сути, есть несколько способа решения задания:
     *
     * 1) распараллеливать вычисление факториала для одного числа
     *
     * 2) распараллеливать вычисления для разных чисел
     *
     * 3) комбинированный
     *
     * При чем вычислив факториал для одного числа, можно запомнить эти данные и использовать их для вычисления другого, что будет гораздо быстрее
     */
    public static HashMap<Integer, BigInteger> map = new HashMap<>();

    public static void main(String[] args) {
        int[] intArr = {3,5,7,8};
        BigInteger[] res = new BigInteger[intArr.length];
        for (int i = 0; i < intArr.length; i++){
            Factorial f = new Factorial(intArr[i]);
            FutureTask<BigInteger> task = new FutureTask<>(f);
            Thread t = new Thread(task);
            t.start();
            try {
                res[i] = task.get();
            }
            catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < intArr.length; i++) {
            System.out.println("Factorial(" + intArr[i] + ") = " + res[i]);
        }
    }
}
