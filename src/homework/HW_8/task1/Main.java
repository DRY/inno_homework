package homework.HW_8.task1;

import java.io.*;
//import com.fasterxml.jackson.core.JsonGenerationException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.alibaba.fastjson.JSON;

public class Main {
    /**
     * Необходимо разработать класс, реализующий следующие методы:
     *
     * void serialize (Object object, String file);
     *
     * Object deSerialize(String file);
     *
     * Методы выполняют сериализацию объекта Object в файл file и десериализацию объекта из этого файла.
     * Обязательна сериализация и десериализация "плоских" объектов (все поля объекта - примитивы, или String).
     */

    public static void main(String[] args) {
        Person p = new Person("Aabaa", "MAN", 125, 65.5);
        p.printPerson();
        serialize(p, "src\\homework\\HW_8\\task1\\person.bin");
        Person newP = (Person) deSerialize("src\\homework\\HW_8\\task1\\person.bin");
        newP.printPerson();
    }

    private static void serialize(Object object, String file) {
        /**
         * Метод выполняет сериализацию объекта Object в файл file
         */
//        ObjectMapper mapper = new ObjectMapper();
//        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
//            oos.writeObject(object);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    private static Object deSerialize(String file) {
        /**
         * Метод выполняет десериализацию Object из file
         */
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            Object o = ois.readObject();
            return o;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
