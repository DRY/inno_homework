package homework.HW_8.task1;

import java.io.Serializable;
import java.util.Locale;
import java.util.Random;

public class Person implements Serializable {
    private static final long serialVersionUID = 2202L;
    private int age;
    private String sex;
    private String name;
    private Double height;

    public Person(String name, String sex, int age, double height){
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.height = height;
    }

    public String getSex(){
        return this.sex;
    }
    public int getAge(){
        return this.age;
    }
    public String getName(){
        return this.name;
    }
    public Double getHeight(){
        return this.height;
    }

    public void printPerson(){
        /**
         * вывод информации о хозяине
         */
        System.out.println(this.name + " - " + this.sex + " - " + this.age);
    }
}

