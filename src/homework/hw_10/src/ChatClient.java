import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.*;
import java.util.Scanner;

public class ChatClient {
    /**
     * Класс для подключения к серверу и отправки сообщений из консоли
     */
    static int[] ports = {5000, 5001, 5002, 5003, 5004};
    static String nickname;

    public static void main(String[] args) {
        Listener listener = new Listener(ports);
        listener.start();
        try (Socket socket = new Socket("127.0.0.1", 4999);
             BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))) {
            System.out.println("Введи nickname");
            Scanner sc = new Scanner(System.in);
            nickname = sc.nextLine();
            System.out.println("Добро пожаловать " + nickname);
            sendMessage(bufferedWriter, "/nickname:" + nickname);
            String message;
            while (!(message = sc.nextLine()).equals("quit")) {
                sendMessage(bufferedWriter, message);
            }
            sendMessage(bufferedWriter, "quit");
            listener.start = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void sendMessage(BufferedWriter bw, String message){
        try {
            bw.write(message);
            bw.newLine();
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
