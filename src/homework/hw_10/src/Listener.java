import java.io.IOException;
import java.net.*;

public class Listener extends Thread {
    /**
     * Класс для получения сообщений от сервера и вывода в консоль
     */
    private int[] ports;
    public static int port;
    public boolean start = true;

    public Listener(int[] ports) {
        this.ports = ports;
    }

    @Override
    public void run() {
        boolean flag = true;
        port = 0;
        int i = 0;
        DatagramSocket ds = null;
        try {
            while (flag){
                try{
                    if (i <= ports.length){
                        port = ports[i];
                        ds = new DatagramSocket(port);
                        flag = false;
                    }
                    else {
                        System.out.println("Все порты заняты");
                    }
                } catch (BindException e){
                    System.out.println("Порт " + port + " занят, попытка переподключения...");
                    flag = true;
                    i++;
                }
            }
            String message;
            while (start) {
                DatagramPacket pack = new DatagramPacket(new byte[1024], 1024);
                ds.receive(pack);
                message = new String(pack.getData());
                if (message.indexOf("/to") > 0){
                    if (message.startsWith("/to " + ChatClient.nickname, message.indexOf(" > ") + 3)) {
                        System.out.println("[Private message]: " + message.replace("/to " + ChatClient.nickname, ""));
                    }
                }
                else
                    System.out.println(message);
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
