import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;

public class ServerClientThread extends Thread {
    /**
     * Класс для обработки полученных сообщений от клиента и отправки другим клиентам
     */
    Socket socket;
    String nickname = "";
    int[] ports = {5000, 5001, 5002, 5003, 5004};
    public ServerClientThread(Socket socket){
        this.socket = socket;
    }

    @Override
    public void run() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()))){
            String message;
            while (!(message = reader.readLine()).equals("quit")) {
                if (message.startsWith("/nickname:")){
                    nickname = message.replace("/nickname:", "");
                }
                else {
                    sendMessage("255.255.255.255", nickname + " > " + message);
                }
            }
            sendMessage("255.255.255.255", nickname + " покинул чат");
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendMessage(String host, String mes){
        try{
            byte[] data = mes.getBytes();
            InetAddress addr = InetAddress.getByName(host);
            for (int i = 0; i < ports.length; i++){
                DatagramPacket pack = new DatagramPacket(data, data.length, addr, ports[i]);
                DatagramSocket ds = new DatagramSocket();
                ds.send(pack);
                ds.close();
            }
        }catch(IOException e){
            System.err.println(e);
        }
    }
}
