import java.io.IOException;
import java.net.ServerSocket;

public class ServerListener extends Thread {
    /**
     * Класс для создания подключения клиента к серверу
     */
    private int port;

    public ServerListener(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(port)){
            while (true){
                ServerClientThread serverClientThread = new ServerClientThread(serverSocket.accept());
                serverClientThread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
