import java.math.BigInteger;
import java.util.*;
import java.util.stream.Stream;

public class Main {
    /**
     * Задание: Перевести одну из предыдущих работ на использование стримов и лямбда-выражений там, где это уместно (возможно, жертвуя производительностью)
     *
     * Дан массив случайных чисел. Написать программу для вычисления факториалов всех элементов массива. Использовать пул потоков для решения задачи.
     * Особенности выполнения:
     * Для данного примера использовать рекурсию - не очень хороший вариант, т.к. происходит большое выделение памяти, очень вероятен StackOverFlow.
     * Лучше перемножать числа в простом цикле при этом создавать объект типа BigInteger
     * По сути, есть несколько способа решения задания:
     * 1) распараллеливать вычисление факториала для одного числа
     * 2) распараллеливать вычисления для разных чисел
     * 3) комбинированный
     * При чем вычислив факториал для одного числа, можно запомнить эти данные и использовать их для вычисления другого, что будет гораздо быстрее
     */
    public static Map<Integer, BigInteger> map = new HashMap<>();

    public static void main(String[] args) {

        Stream.of(3, 5, 7, 8)
                .forEachOrdered(Main::getFactorial);
    }

    static void getFactorial(int num){
        new Thread(() -> {
            BigInteger result = BigInteger.valueOf(1);
            Set<Integer> keys = map.keySet();
            int j = 1;
            for (int i = 1; i <= num; i++) {
                for (int k : keys) {
                    if (k == i) {
                        result = map.get(k);
                        j = k + 1;
                    }
                }
            }
            for (; j <= num; j++) {
                result = result.multiply(BigInteger.valueOf(j));
            }
            map.put(num, result);
            System.out.println(Thread.currentThread().getName() + " - Factorial(" + num + ") = " + result);
        }).start();
    }
}
