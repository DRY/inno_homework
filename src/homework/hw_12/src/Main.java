import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    /**
     * Задание 1.     Необходимо создать программу, которая продемонстрирует утечку памяти в Java. При этом объекты должны не только создаваться,
     * но и периодически частично удаляться, чтобы GC имел возможность очищать часть памяти.
     * Через некоторое время программа должна завершиться с ошибкой OutOfMemoryError c пометкой Java Heap Space.
     * Задание 2.     Доработать программу так, чтобы ошибка OutOfMemoryError возникала в Metaspace /Permanent Generation
     *
     * 1: -XX:+UseSerialGC -Xmx10m
     * Metaspace: -XX:MaxMetaspaceSize=2m -XX:+UseSerialGC -Xmx15m
     */
    private static final int LOOP_COUNT = 100_000_000;

    public static void main(String[] args) throws InterruptedException {
        List<String> list = new ArrayList<>();

        Random random = new Random();
        for (int i = 0; i < LOOP_COUNT; i++) {
            String str = "" + random.nextInt();
            list.add(str);
            if (i % 10 == 0) {
                Thread.sleep(1);
                list.remove(0);
            }
        }
    }
}
