import ConnectionManager.ConnectionManager;
import ConnectionManager.ConnectionManagerJdbcImpl;
import dao.HwDao;
import pojo.Role;

public class CreateDefaultRole {

    public static void main(String[] args) {
        ConnectionManager connectionManager = ConnectionManagerJdbcImpl.getInstance();
        HwDao hwDao = new HwDao(connectionManager);
        Role roleAdministration = new Role(null, "Administration", "Роль Administration");
        Role roleClients = new Role(null, "Clients", "Роль Clients");
        Role roleBilling = new Role(null, "Billing", "Роль Billing");
        hwDao.addDefaultRoles(roleAdministration, roleClients, roleBilling);
    }
}
