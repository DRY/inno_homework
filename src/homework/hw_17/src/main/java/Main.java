import ConnectionManager.ConnectionManager;
import ConnectionManager.ConnectionManagerJdbcImpl;
import dao.HwDao;
import pojo.User;
import pojo.UserRole;

public class Main {
    /**
     * Взять за основу ДЗ_15,
     * покрыть код логированием
     * в основных блоках try покрыть уровнем INFO
     * с исключениях catch покрыть уровнем ERROR
     * настроить конфигурацию логера, что бы все логи записывались в БД, таблица LOGS,
     * колонки ID, DATE, LOG_LEVEL, MESSAGE, EXCEPTION
     */

    public static void main(String[] args) {
        ConnectionManager connectionManager = ConnectionManagerJdbcImpl.getInstance();
        HwDao hwDao = new HwDao(connectionManager);
//        hwDao.addTwoAdminUser();
//        addUser(hwDao);
//        hwDao.testTransaction();
        System.out.println(hwDao.getUserByLoginIdAndName(1, "Admin"));

    }

    static void addUser(HwDao hwDao) {
        User user = new User(null, "test", "01.01.2019", 3, "Москва", "test@mail.ru", "test user");
        hwDao.addUserRow(user);
        User user1 = hwDao.getUserByLoginIdAndName(user.getLogin_ID(), user.getName());
        UserRole userRole = new UserRole(null, user1.getId(), 2);
        hwDao.addUserRoleRow(userRole);
    }

}
