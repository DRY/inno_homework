package dao;

import ConnectionManager.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pojo.Role;
import pojo.User;
import pojo.UserRole;

import java.sql.*;

public class HwDao {
    private static final Logger LOGGER = LogManager.getLogger(HwDao.class);
    public static final String INSERT_INTO_USER = "INSERT INTO hw_15.\"USER\" VALUES (DEFAULT, ?, to_date(?, 'dd.mm.yyyy'), ?, ?, ?, ?)";
    public static final String INSERT_INTO_ROLE = "INSERT INTO hw_15.\"ROLE\" VALUES (DEFAULT, ?, ?)";
    public static final String INSERT_INTO_USER_ROLE = "INSERT INTO hw_15.\"USER_ROLE\" VALUES (DEFAULT, ?, ?)";
    public static final String SELECT_USER = "SELECT * from hw_15.\"USER\" where login_id = ? and name = ?";
    private ConnectionManager connectionManager;

    public HwDao(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public boolean addUserRow(User user) {
        LOGGER.info("Добавление USER - " + user.toString());
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INTO_USER)) {
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getBirthday());
            preparedStatement.setInt(3, user.getLogin_ID());
            preparedStatement.setString(4, user.getCity());
            preparedStatement.setString(5, user.getEmail());
            preparedStatement.setString(6, user.getDescription());
            preparedStatement.execute();
        } catch (SQLException e) {
            LOGGER.error("Не удалось добавить запись в таблицу User");
            return false;
        }
        return true;
    }

    public boolean addRoleRow(Role role) {
        LOGGER.info("Добавление ROLE - " + role.toString());
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INTO_ROLE)) {
            preparedStatement.setString(1, role.getName());
            preparedStatement.setString(2, role.getDescription());
            preparedStatement.execute();
        } catch (SQLException e) {
            LOGGER.error("Не удалось добавить запись в таблицу Role");
            return false;
        }
        return true;
    }

    public boolean addUserRoleRow(UserRole userRole) {
        LOGGER.info("Добавление USER_ROLE - " + userRole.toString());
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INTO_USER_ROLE)) {
            preparedStatement.setInt(1, userRole.getUser_id());
            preparedStatement.setInt(2, userRole.getRole_id());
            preparedStatement.execute();
        } catch (SQLException e) {
            LOGGER.error("Не удалось добавить запись в таблицу User_Role");
            return false;
        }
        return true;
    }

    public boolean addTwoAdminUser() {
        LOGGER.info("Добавление двух администраторов Admin и SYS_Admin");
        try (Connection connection = connectionManager.getConnection();
             Statement statement = connection.createStatement()) {
            statement.addBatch("INSERT INTO hw_15.\"USER\" VALUES (1, 'Admin', to_date('01.01.2000', 'dd.mm.yyyy'), 1, null, null, 'Administrator DB')");
            statement.addBatch("INSERT INTO hw_15.\"USER\" VALUES (2, 'SYS_Admin', to_date('01.01.2000', 'dd.mm.yyyy'), 2, null, null, 'System Administrator')");
            statement.addBatch("INSERT INTO hw_15.\"USER_ROLE\" VALUES (DEFAULT, 1, 1)");
            statement.addBatch("INSERT INTO hw_15.\"USER_ROLE\" VALUES (DEFAULT, 2, 1)");
            statement.executeBatch();
        } catch (SQLException e) {
            LOGGER.error("Не удалось добавить записи в таблицы User и User_Role");
            return false;
        }
        return true;
    }

    public User getUserByLoginIdAndName(int login_id, String name) {
        LOGGER.info("Поиск записи по login_id = " + login_id + " и name = " + name);
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER)) {
            preparedStatement.setInt(1, login_id);
            preparedStatement.setString(2, name);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return new User(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getInt(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getString(7)
                    );
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Не удалось найти User");
        }
        return null;
    }

    public boolean addDefaultRoles(Role roleAdministration, Role roleClients, Role roleBilling) {
        LOGGER.info("Добавление дефолтных ролей");
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO hw_15.\"ROLE\" VALUES (DEFAULT, ?, ?)")) {
            preparedStatement.setString(1, roleAdministration.getName());
            preparedStatement.setString(2, roleAdministration.getDescription());
            preparedStatement.addBatch();
            preparedStatement.setString(1, roleClients.getName());
            preparedStatement.setString(2, roleClients.getDescription());
            preparedStatement.addBatch();
            preparedStatement.setString(1, roleBilling.getName());
            preparedStatement.setString(2, roleBilling.getDescription());
            preparedStatement.addBatch();
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            LOGGER.error("Не удалось добавить записи в таблицу Role");
            return false;
        }
        return true;
    }

    public void testTransaction() {
        LOGGER.info("Обновление записей в USER");
        try (Connection connection = connectionManager.getConnection();
             Statement statement = connection.createStatement()) {
            connection.setAutoCommit(false);
            statement.execute("UPDATE hw_15.\"USER\" set name = 'test_trans' where id = 4");
            Savepoint savepoint = connection.setSavepoint();
            statement.execute("UPDATE hw_15.\"USER\" set name = 'test_trans_savepoint' where id = 4");
            connection.rollback(savepoint);
            connection.commit();
        } catch (SQLException e) {
            LOGGER.error("Не удалось обновить записи в БД");
        }
    }
}