import ConnectionManager.ConnectionManager;
import ConnectionManager.ConnectionManagerJdbcImpl;
import dao.HwDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MainTest {

    private static final Logger LOGGER = LogManager.getLogger(MainTest.class);

    private static Main main;
    private static HwDao hwDao;
    private static ConnectionManager connectionManager;


    @BeforeAll
    static void tearDownAll() {
        main = new Main();
        connectionManager = ConnectionManagerJdbcImpl.getInstance();
        hwDao = new HwDao(connectionManager);
    }

    @AfterAll
    static void setUpAll() {

    }

    @Test
    void main() {
        assertDoesNotThrow(() -> main.addUser(hwDao));
    }

    @Test
    void mainWithException() {
        assertThrows(NullPointerException.class, () -> main.addUser(null));
    }
}
