package dao;

import ConnectionManager.ConnectionManager;
import org.junit.jupiter.api.*;
import ConnectionManager.ConnectionManagerJdbcImpl;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import pojo.Role;
import pojo.User;
import pojo.UserRole;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class HwDaoTest {

    private static HwDao hwDao;
    private static ConnectionManager connectionManager;
    private static Connection connection;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private ResultSet resultSet;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @BeforeAll
    static void tearDownAll() {
        connectionManager = mock(ConnectionManagerJdbcImpl.class);
        connection = mock(Connection.class);
        hwDao = new HwDao(connectionManager);
    }

    @Test
    void addUserRow() throws SQLException {
        when(connectionManager.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(hwDao.INSERT_INTO_USER)).thenReturn(preparedStatement);
        String name = "test";
        String birthday = "01.01.2019";
        int login_ID = 3;
        String city = "Москва";
        String email = "test@mail.ru";
        String description = "test user";
        User user = new User(null, name, birthday, login_ID, city, email, description);

        boolean result = hwDao.addUserRow(user);

        verify(connection, times(1)).prepareStatement(hwDao.INSERT_INTO_USER);
        verify(preparedStatement, times(1)).setString(1, name);
        verify(preparedStatement, times(1)).setString(2, birthday);
        verify(preparedStatement, times(1)).setInt(3, login_ID);
        verify(preparedStatement, times(1)).setString(4, city);
        verify(preparedStatement, times(1)).setString(5, email);
        verify(preparedStatement, times(1)).setString(6, description);
        verify(preparedStatement, times(1)).execute();
        assertTrue(result);
    }

    @Test
    void addRoleRow() throws SQLException {
        when(connectionManager.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(hwDao.INSERT_INTO_ROLE)).thenReturn(preparedStatement);
        Role role = new Role(null, "test", "test role");

        boolean result = hwDao.addRoleRow(role);

        verify(connection, times(1)).prepareStatement(hwDao.INSERT_INTO_ROLE);
        verify(preparedStatement, times(2)).setString(anyInt(), anyString());
        verify(preparedStatement, times(1)).execute();
        assertTrue(result);
    }

    @Test
    void addUserRoleRow() throws SQLException {
        when(connectionManager.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(hwDao.INSERT_INTO_USER_ROLE)).thenReturn(preparedStatement);
        UserRole userRole = new UserRole(null, 1, 1);

        boolean result = hwDao.addUserRoleRow(userRole);

        verify(connection, times(1)).prepareStatement(hwDao.INSERT_INTO_USER_ROLE);
        verify(preparedStatement, times(2)).setInt(anyInt(), anyInt());
        verify(preparedStatement, times(1)).execute();
        assertTrue(result);
    }

    @Test
    @Disabled
    void addTwoAdminUser() {
        boolean result = assertDoesNotThrow(() -> hwDao.addTwoAdminUser());

        assertTrue(result);
    }

    @Test
    void getUserByLoginIdAndName() throws SQLException {
        when(connectionManager.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(hwDao.SELECT_USER)).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt(anyInt())).thenReturn(1);
        when(resultSet.getString(anyInt())).thenReturn("Admin", "2000-01-01", null, null, "Administrator DB");
        int login_id = 1;
        String name = "Admin";

        User result = hwDao.getUserByLoginIdAndName(login_id, name);

        verify(connection, times(1)).prepareStatement(hwDao.SELECT_USER);
        verify(preparedStatement, times(1)).setInt(1, login_id);
        verify(preparedStatement, times(1)).setString(2, name);
        verify(preparedStatement, times(1)).executeQuery();
        verify(resultSet, times(1)).getInt(1);
        verify(resultSet, times(1)).getString(2);
        verify(resultSet, times(1)).getString(3);
        verify(resultSet, times(1)).getInt(4);
        verify(resultSet, times(1)).getString(5);
        verify(resultSet, times(1)).getString(6);
        verify(resultSet, times(1)).getString(7);
        assertEquals(result.getId(), 1);
        assertEquals(result.getName(), name);
        assertEquals(result.getBirthday(), "2000-01-01");
        assertEquals(result.getLogin_ID(), login_id);
        assertNull(result.getCity());
        assertNull(result.getEmail());
        assertEquals(result.getDescription(), "Administrator DB");
    }
}
