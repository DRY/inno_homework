package dao;


import pojo.Mobile;
import pojo.User;

import java.util.Collection;

public interface MobileDao {
    boolean addMobile(Mobile mobile);

    Mobile getMobileById(Integer id);

    boolean updateMobileById(Mobile mobile);

    boolean deleteMobileById(Integer id);

    void createTable();

    Collection<Mobile> getAllMobile();

    User getUser(String login, String pass);

    User getUserById(Integer id);

    boolean updateUserById(User user);

    User getUserByLogin(String login);
}
