package servlet;

import dao.MobileDao;
import pojo.Mobile;
import pojo.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/auth")
public class AuthServlet extends HttpServlet {

    private MobileDao mobileDao;

    @Override
    public void init() throws ServletException {
        mobileDao = (MobileDao) getServletContext().getAttribute("dao");
        super.init();
    }

//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        req.setAttribute("PageTitle", "New Mobiles");
//        req.setAttribute("PageBody", "form.jsp");
//        req.getRequestDispatcher("/layout.jsp")
//            .forward(req, resp);
//    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.setCharacterEncoding("utf-8");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        User user = mobileDao.getUser(login, password);
        if (user == null) {
            throw new ServletException("Пользователь " + login + " не найден");
        }
        req.setAttribute("login_id", user.getId());
        resp.sendRedirect(req.getContextPath() + "/mainmenu?login_id=" + user.getId());
    }
}
