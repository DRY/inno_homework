package servlet;

import dao.MobileDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pojo.Mobile;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@WebServlet("/mainmenu")
public class MainMenuServlet extends HttpServlet {
    private Logger logger = LoggerFactory.getLogger(AppContextListener.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("PageTitle", "Main menu");
        req.setAttribute("PageBody", "mainmenu.jsp");
        req.getRequestDispatcher("/layout.jsp")
            .forward(req, resp);
    }
}
