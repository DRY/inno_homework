package servlet;

import dao.MobileDao;
import pojo.Mobile;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/updatemobile")
public class UpdateMobileServlet extends HttpServlet {

    private MobileDao mobileDao;

    @Override
    public void init() throws ServletException {
        mobileDao = (MobileDao) getServletContext().getAttribute("dao");
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String mobileId = req.getParameter("id");
        if (mobileId == null) {
            throw new ServletException("Missing parameter id");
        }
        Mobile mobile = mobileDao.getMobileById(Integer.valueOf(mobileId));
        if (mobile == null) {
            resp.setStatus(404);
            req.getRequestDispatcher("/notfound.jsp").forward(req, resp);
            return;
        }
        req.setAttribute("mobile", mobile);
        req.setAttribute("PageTitle", "Edit Mobiles");
        req.setAttribute("PageBody", "formUpdate.jsp");
        req.getRequestDispatcher("/layout.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.setCharacterEncoding("utf-8");
        String mobileIds = req.getParameter("id");
        if (mobileIds == null) {
            throw new ServletException("Missing parameter id");
        }
        int mobileId = Integer.parseInt(mobileIds);
        Mobile mobile = mobileDao.getMobileById(mobileId);
        mobile.setModel(req.getParameter("model"));
        mobile.setPrice(Integer.parseInt(req.getParameter("price")));
        mobile.setManufacturer(req.getParameter("manufacturer"));
        mobileDao.updateMobileById(mobile);

        resp.sendRedirect(req.getContextPath() + "/allmobiles");
    }
}
