package servlet;

import dao.MobileDao;
import pojo.Mobile;
import pojo.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/updateuser")
public class UpdateUserServlet extends HttpServlet {

    private MobileDao mobileDao;

    @Override
    public void init() throws ServletException {
        mobileDao = (MobileDao) getServletContext().getAttribute("dao");
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        String login = req.getParameter("login");
        User user = mobileDao.getUserByLogin(login);
        if (user == null) {
            throw new ServletException("Пользователь " + login + " не найден");
        }
        req.setAttribute("user", user);
        req.setAttribute("PageTitle", "Edit User");
        req.setAttribute("PageBody", "formUpdateUser.jsp");
        req.getRequestDispatcher("/layout.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.setCharacterEncoding("utf-8");
        String userIds = req.getParameter("login_id");
        if (userIds == null) {
            throw new ServletException("Missing parameter login_id");
        }
        int userId = Integer.parseInt(userIds);
        User user = mobileDao.getUserById(userId);
        user.setEmail(req.getParameter("email"));
        user.setPhone(req.getParameter("phone"));
        mobileDao.updateUserById(user);

        resp.sendRedirect(req.getContextPath() + "/mainmenu");
    }
}
