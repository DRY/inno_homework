<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<h1>Авторизация</h1>
<form method="post" action="${pageContext.request.contextPath}/mainmenu" autocomplete="off">
    <div class="form-group">
        <label for="login">LOGIN</label>
        <input name="login" type="text" class="form-control" id="login" value="">
    </div>
    <div class="form-group">
        <label for="password">PASSWORD</label>
        <input name="password" type="text" class="form-control" id="password" value="">
    </div>
    <button type="submit" class="btn btn-primary">Авторизоваться</button>
</form>

