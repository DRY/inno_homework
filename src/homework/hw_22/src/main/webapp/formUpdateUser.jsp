<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<h1>Update user</h1>
<form method="post" action="${pageContext.request.contextPath}/updateuser" autocomplete="off">
    <div class="form-group">
        <label for="login_id">ID = ${user.login}</label>
        <input name="login_id" type="hidden" class="form-control" id="login_id" value="${user.id}">
    </div>
    <div class="form-group">
        <label for="email">email</label>
        <input name="email" type="text" class="form-control" id="email" value="${user.email}">
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input name="phone" type="text" class="form-control" id="phone" value="${user.phone}">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

