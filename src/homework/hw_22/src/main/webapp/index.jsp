<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="/WEB-INF/date.tld" prefix="datetag" %>
<%@taglib prefix="myTags" tagdir="/WEB-INF/tags" %>

<myTags:template>
    <jsp:attribute name="header">
        <h1>Mobiles</h1>
        (<datetag:DateTag plus="1"/>)
    </jsp:attribute>
    <jsp:body>
        <h1>Авторизация</h1>
        <form method="post" action="${pageContext.request.contextPath}/auth" autocomplete="off">
            <div class="form-group">
                <label for="login">LOGIN</label>
                <input name="login" type="text" class="form-control" id="login" value="">
            </div>
            <div class="form-group">
                <label for="password">PASSWORD</label>
                <input name="password" type="text" class="form-control" id="password" value="">
            </div>
            <button type="submit" class="btn btn-primary">Авторизоваться</button>
        </form>
    </jsp:body>
</myTags:template>