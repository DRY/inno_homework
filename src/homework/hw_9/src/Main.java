import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        System.out.println("Введи код для метода doWork");
        Scanner sc = new Scanner(System.in);
        StringBuilder sb = new StringBuilder();
        String temp = sc.nextLine();
        while (!temp.equals("")) {
            sb.append("        " + temp + "\n");
            temp = sc.nextLine();
        }
        StringBuilder sbRead = new StringBuilder();
        StringBuilder sbRead2 = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(".\\src\\SomeClass.java")))) {
            String tmp;
            while (!(tmp = br.readLine()).equals("    public void doWork() {")) {
                sbRead.append(tmp + "\n");
            }
            sbRead.append(tmp + "\n");
            while (!(tmp = br.readLine()).equals("    }")) { }
            sbRead2.append(tmp + "\n");
            while ((tmp = br.readLine()) != null) {
                sbRead2.append(tmp + "\n");
            }
            System.out.println(sbRead.toString());
            System.out.println(sbRead2.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder finalStr = new StringBuilder();
        try (OutputStreamWriter fs = new OutputStreamWriter(new FileOutputStream(".\\src\\SomeClass.java"))) {
            finalStr.append(sbRead.toString());
            finalStr.append(sb.toString());
            finalStr.append(sbRead2.toString());
            fs.write(finalStr.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        String cmd = "javac -sourcepath ./src -d ./out/production/hw_9 ./src/SomeClass.java";
        Runtime.getRuntime().exec(cmd).waitFor();
        useCustomClassLoader();
    }

    private static void useCustomClassLoader() throws Exception {
        ClassLoader cl = new MyClassLoader();
        Class<?> loadSomeClass = cl.loadClass("SomeClass");
        Worker someClass = (Worker) loadSomeClass.newInstance();
        someClass.doWork();
    }


}
